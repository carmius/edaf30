#include <ctime>  // time and localtime
#include "date.h"
#include <iostream>
#include <istream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <iomanip>
#include <chrono>
#include <array>
using std::istream;
using std::vector;
using std::istringstream;
using std::string;
int Date::daysPerMonth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
const std::string Date::DAYS[] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

struct date_io_fmt : std::ctype<char> {

	date_io_fmt(): std::ctype<char>(make_table()) {} // call base cons, med make_table()
	static const mask* make_table() {
		static std::vector<std::ctype_base::mask>
		 mask(std::ctype<char>::table_size, std::ctype_base::mask());
		 mask['-'] = std::ctype_base::space; 
		 return &mask[0];
	}
};

Date::Date() {
	time_t timer = time(0); // time in seconds since 1970-01-01
	tm* locTime = localtime(&timer); // broken-down time
	year = 1900 + locTime->tm_year;
	month = locTime->tm_mon;
	day = locTime->tm_mday;
}

Date::Date(int y, int m, int d) : year(y), month(m-1), day(d-1) {
}

int Date::getYear() const {
	return year;
}
int Date::getMonth() const {
	return month+1;
}

int Date::getDay() const {
	return day+1;
}

void Date::next() {
	day++;
	if(getDay() > daysPerMonth[month]) {
		month++;
		day = 0;
		if(getMonth() > (int)(sizeof(daysPerMonth)/(sizeof(*std::end(daysPerMonth)))))
		{
			year++;
			month = 0;
		}	
	}
		
}

std::istream& operator>>(istream& in,Date& date) {
	std::string read;
	in >> read;
	std::istringstream ss(read);
	std::tm time_struct = {};
	ss >> std::get_time(&time_struct, "%Y-%m-%d");
	if(ss.fail()) in.setstate(std::ios_base::badbit);	
	else {
		date.day = time_struct.tm_mday-1;
		date.month = time_struct.tm_mon;
		date.year = time_struct.tm_year+1900;
		in.setstate(std::ios_base::eofbit);
	}
	return (in);
}

std::ostream& operator<<(std::ostream& out, const Date& d) {
	out << std::setw(4) << std::setfill('0') << d.getYear();
	out << "-" << std::setw(2) <<std::setfill('0') <<d.getMonth() << "-";
	out << std::setw(2) <<std::setfill('0') <<d.getDay();
	return out;	
}
