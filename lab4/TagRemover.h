#include <istream>
#include <memory>
#include <regex>
#include <map>

class TagRemover { 
 public:
	TagRemover(std::iostream& in);
	~TagRemover() {}; 
	std::string strip_tags(std::string original);
	void print(std::ostream& out); 
 private:
	std::string content_tagstripped;
	// std::stringstream ss;
	std::map<std::string, char> ch_map;
	bool setupSpecialChar();
};
