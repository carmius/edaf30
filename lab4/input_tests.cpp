#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <array>
#include <locale>
#include <exception>
#include <iomanip>
#include <ctime>
struct date_io_fmt : std::ctype<char> {

	date_io_fmt(): std::ctype<char>(make_table()) {} // call base cons, med make_table()
	static const mask* make_table() {
		static std::vector<std::ctype_base::mask>
		 rc(std::ctype<char>::table_size, std::ctype_base::mask());
		
		 rc['-'] = std::ctype_base::space; 
		 rc['.'] = std::ctype_base::space; 
		 rc[':'] = std::ctype_base::space; 
		 rc['/'] = std::ctype_base::space; 
		 return &rc[0];
	}
};
void test_simple() {
	std::cout << "Hello";
	int d,m,y;
	d=m=y=0;	
	std::string s;
	std::cin >> s;
	std::cout << "read string: " << s << "\n";
	std::istringstream isstream(s);
	if(isstream) {
		isstream >> d >> m >> y;
	}
	else {
		std::cout << "Error!\n";
	}	
	std::cout <<  d << "," << m << "," << y;
	std::string str;
} 
void try_get_date(const std::string& s)
{
   std::cout <<  "Parsing the date out of '" << s <<
	                     "' in the locale " << std::locale().name() << '\n';

   std::string newstr{s};
	// initialize str with s   
    std::istringstream str(s);
    std::istringstream test(newstr);
    std::tm t2;
    test >> std::get_time(&t2, "%Y-%M-%D");
	std::cout << t2.tm_wday;
    std::ios_base::iostate catch_err = std::ios_base::goodbit;					// error flagga som vi kollar på för fails
 
    std::tm t; // time-struc
    std::istreambuf_iterator<char> ret =
    /* från cppreference
     * std::time_get<char> parses narrow string representations of date and time
     * use_facet() returnerar en reference till en facet, implementerad av str.getloc() i det här fallet, våran str-istream
     * std::time_get::get_date() :
     *  beg 	- 	iterator designating the start of the sequence to parse		(istreambuf_iterator<char>(str))
     *  end 	- 	one past the end iterator for the sequence to parse				(...)
     *  str 	- 	a stream object that this function uses to obtain locale facets when needed,	(str)
     *  		e.g. std::ctype to skip whitespace or std::collate to compare strings		
     *  err 	- 	stream error flags object that is modified by this function to indicate errors	(err)
     *  t 	- 	pointer to the std::tm object that will hold the result of this function call 
     */
        std::use_facet<std::time_get<char> >(str.getloc())
					.get_date(std::istreambuf_iterator<char>(str), // begin iterator, som extraherar objekt från den givna strömmen, str i det här fallet
						  std::istreambuf_iterator<char>(),	// end iterator, istream_iterators(), default cons, som resulterar i en iterator som pekar på end() 
						  str,					// vad gör strömobjektet här
						  catch_err,					// en std::ios_base::goodbit, varför?
						  &t);					// time-struct'en som den parsade strängen slutligen landar i


    str.setstate(catch_err);							// sätt ström till "ok" 
    if(str) {										// om ström: "ok"
            std::cout << "Day: "     << t.tm_mday << ' '				// skriv ut dag
                      << "Month: " << t.tm_mon + 1 << ' '				// skriv ut månad
                      << "Year: "  << t.tm_year + 1900 << '\n';				// skriv ut år, sedan 1900
        } else {									// annars, oj oj oj
	        std::cout <<  "Parse failed. Unparsed string: ";			// ...
	        std::copy(ret, std::istreambuf_iterator<char>(),			// "koppla ihop iteratorn ret, med iteratorn, kopplad till cout"
				                  std::ostreambuf_iterator<char>(std::cout));
	        std::cout <<  '\n';
	    }

}
int main(int argc, char** argv) {
	if(argc > 1) {

	switch(*argv[1]) {
		case '1':
			test_simple();
			break;
		case '2':
			break;
		case '3':
			break;
		default:
			try_get_date("02/01/2013");
		       	try_get_date("2013-01-10");
		       	try_get_date("2013/01/10");
			break;

	}
}
    	
    return 0;
}





