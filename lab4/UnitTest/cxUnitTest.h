//
// Created by cx on 2016-12-01.
//

#ifndef LAB4_CXUNITTEST_H
#define LAB4_CXUNITTEST_H

#include <memory>

namespace cxUT {

    template <typename T>
    class Equals {
        const T val;
    public:
        Equals(const T& v) :val{v} {}
        bool operator()(const T& x) {
            return x == val;
        }
    };

    template <typename T>
    class cxUnitTest<T> {
        cxUnitTest(std::shared_ptr<T> test_ptr); // since we're going to pass struct around
        ~cxUnitTest();
    };


    template <typename T>
    // vi passar explicit in värdet, AssertValue, att jämföra med utgången på testet
    // som genererar shared_ptr'n test_ptr, dvs resultatet, som ska jämföras.
    // så, dvs, AssertValue == test_ptr->value
    bool AssertEq(T AssertValue, std::shared_ptr<T> test_ptr) {
        test_ptr->() == AssertValue;
    }
}


#endif //LAB4_CXUNITTEST_H
