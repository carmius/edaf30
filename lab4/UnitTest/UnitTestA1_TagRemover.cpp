#include <iostream>
#include <istream>
#include <sstream>
#include <fstream>
#include <regex>
#include <map>

using std::string;
using std::map;
constexpr int DEBUG=0;
string strip_tags(std::string& original) {
    std::ostringstream result{""};
    std::istringstream iss(original);

    short tag_count = 0;
    for(string::iterator ch = original.begin(); ch != original.end(); ch++)
    {
        if(*ch != '<' && tag_count == 0) { // if we find opening tag
            if(*ch == '&') {                // start processing possible special char
                std::ostringstream inoss;
                auto inner_ch_it = ch;
                int MAX_RNG = 5;            // special character-compositions have max length
                for(int x = 0; *inner_ch_it != ';' && x < MAX_RNG; inner_ch_it++, x++) { // look for ; or check for MAX_RNG
                    inoss << *inner_ch_it;
                }
                if(ch_map.find(inoss.str()) != ch_map.end()) // if special character exists
                {
                    result << ch_map[inoss.str()];
                }
                else
                {
                    result << inoss.str() << *inner_ch_it;  // we have to add the last character, or we become 1 short
                }
                ch = inner_ch_it;   // set outer iterator to inner, inner is destroyed
            }
            else result << *ch; // if not '&', just output the damn thing
        }
        else if(*ch == '<') {   //
            tag_count++;
        }
        else if(*ch == '>' && tag_count > 0){
            tag_count--;
        }
    }
    return result.str();
}

bool test_tagstrip(string input, string expected) {
    auto result = strip_tags(input);
    return result == expected;
}



int main() {
	//std::fstream f;
	// f.open("../htmlstrip/original.html");
    // std::ostringstream oss;

	/*if(f.is_open())
	{
        oss << f;
 	    TagRemover tr(  std::cin.read);
	}
	
	std::ifstream result;
	result.open("../htmlstrip/stripped.html");
    */

    std::string s{"<html><body>I am the greatest, show &lt;3 \n"
                          "&amp; kneel before Zod! </body></html>"};
    std::string correct{"I am the greatest, show <3 \n"
                                "& kneel before Zod! "};
    std::string s2{"<html><body>I am the greatest, show &ltttttt;3 \n"
                           "&amp; kneel before Zod! </body></html>"};
    std::string correct2{"I am the greatest, show &ltttttt;3 \n"
                                 "& kneel before Zod! "};
    std::string s3{"html><body>I am the greatest, &nbsp;&nbsp;&nbsp;show &lt;3 \n"
                           "&amp; kneel before Zod! </body></html>"};
    std::string correct3{"html>I am the greatest,    show <3 \n"
                                 "& kneel before Zod! "};
    std::string s4{"<<html><body>I am the greatest, &nbsp show &lt;3 \n"
                          "&amp; kneel before Zod! </body></html>"};
    std::string correct4{""};
    std::cout << "Test 1: " << (test_tagstrip(s, correct) ? "\t\t\tPASSED" : "\t\t\tFAILED") << std::endl;
    std::cout << "Test 2: " << (test_tagstrip(s2, correct2) ? "\t\t\tPASSED" : "\t\t\tFAILED") << std::endl;
    std::cout << "Test 3: " << (test_tagstrip(s3, correct3) ? "\t\t\tPASSED" : "\t\t\tFAILED") << std::endl;
    std::cout << "Test 4: " << (test_tagstrip(s4, correct4) ? "\t\t\tPASSED" : "\t\t\tFAILED") << std::endl;
	return 0;
}
