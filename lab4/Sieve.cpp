#include <string>
#include <iostream>
#include <algorithm>
int main() { 
    const size_t SIEVE_LN = 15000000;
    std::string sieve(SIEVE_LN, 'P');
    for(int i = 0; i < SIEVE_LN; i++) {
        sieve[0] = 'P';
    }
    sieve[0] = sieve[1] = 'C';
    
    size_t mlp = 2;
    while(mlp <= SIEVE_LN) {
        for(size_t step = 1; step*mlp <= SIEVE_LN-1; step+=1) {
            sieve[step*mlp] = 'C';
        }
        mlp = sieve.find_first_of("P");
        std::cout << mlp << ", ";   
    }
    // std::cout << sieve;
}

