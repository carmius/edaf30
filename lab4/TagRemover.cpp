#include "TagRemover.h"
#include <iostream>
#include <vector>
TagRemover::TagRemover(std::iostream& in) {
    setupSpecialChar();
    std::stringstream process_this;
    process_this << in.rdbuf();
    content_tagstripped = strip_tags(process_this.str());
}

bool TagRemover::setupSpecialChar() {
    ch_map["&lt"] = '<';
    ch_map["&gt"] = '>';
    ch_map["&nbsp"] = ' ';
    ch_map["&amp"] = '&';
    return true;
}

std::string TagRemover::strip_tags(std::string original) {
    std::stringstream result{""};
    std::stringstream iss(original);
    
    short tag_count = 0;
    for(std::string::iterator ch = original.begin(); ch != original.end(); ch++)
    {
        if(*ch != '<' && tag_count == 0) { // if we find opening tag
            if(*ch == '&') {                // start processing possible special char
                std::ostringstream inoss;
                auto inner_ch_it = ch;
                int MAX_RNG = 5;            // special character-compositions have max length
                for(int x = 0; *inner_ch_it != ';' && x < MAX_RNG; inner_ch_it++, x++) { // look for ; or check for MAX_RNG
                    inoss << *inner_ch_it;
                }
                if(ch_map.find(inoss.str()) != ch_map.end()) // if special character exists
                {
                    result << ch_map[inoss.str()];
                }
                else
                {
                    result << inoss.str() << *inner_ch_it;  // we have to add the last character, or we become 1 short
                }
                ch = inner_ch_it;   // set outer iterator to inner, inner is destroyed
            }
            else result << *ch; // if not '&', just output the damn thing
        }
        else if(*ch == '<') {   //
            tag_count++;
        }
        else if(*ch == '>' && tag_count > 0){
            tag_count--;
        }
    }
    std::string re;
    result >> re;
    return result.str();
}
void TagRemover::print(std::ostream& out) {
    std::cout << "The number you have called, can't be reached'" << std::endl;
    std::cout << content_tagstripped;
    out << content_tagstripped;
}