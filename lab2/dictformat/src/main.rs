fn main() {
    use std::io::Read;
    use std::io::Write;
    use std::thread;
    use std::sync::mpsc::{Sender, Receiver};
    let cores: usize = 8;
    let mut outputname: String = String::new();
    let mut dict = String::new();
    dict = "test".to_owned();
    outputname.push_str("default.dict");
    let mut setuparg: Vec<String> = std::env::args().collect();
    if setuparg.len() > 2 {
        outputname =  setuparg.pop().unwrap();
        dict = setuparg.pop().unwrap();
    }
    else if setuparg.len() < 3 {
       dict = setuparg.pop().unwrap();  
    }
    println!("{:?}", &dict);
    
    let file = std::fs::OpenOptions::new()
                                .read(true)
                                .write(false)
                                .open(dict);
    
    let mut input: String = String::new();
    let mut f: std::fs::File = file.unwrap();
    let whatever = f.read_to_string(&mut input);
 
    match whatever {
        Ok(value) =>  println!("Read file contents, file size: {:?} KB", value),
        Err(err) => println!("2 22 Error occured while reading contents: {:?}", err),
    }
    
    let sz = input.len();
    let first = 0usize;
    let second = &sz/(cores as usize);
    let third =  &sz/(cores as usize)*2usize;
    let fourth = &sz/(cores as usize)*3usize;
    let end =  &sz/(cores as usize)*4usize;
    use std::sync::mpsc::SyncSender;
    let ranges: Vec<(usize, usize)> = vec!((first, second), (second, third), (third, fourth), (fourth, end));
    let mut ch_output: String = String::new(); 
        let (tx, rx): (SyncSender<(usize, String)>, Receiver<(usize, String)>) = std::sync::mpsc::sync_channel(4);
        // in here lies the problem
        for i in 0..cores {
            let thread_tx = tx.clone();
            let b: Box<(usize, String)> = Box::new((i, input[ranges[i].0 .. ranges[i].1].to_owned()));
            thread::spawn(move|| {
                thread_tx.send(
                 (i, b.1    
                 .lines()
                 .map(|x| Trigram::new(x).to_string())
                 .collect())
            );});
        }
        let mut destination: Vec<Box<String>> = Vec::with_capacity(cores+1);
        use std::ops::Index;
        for i in 0..cores {
            let package: (usize, String) = rx.recv().unwrap();
            destination.insert(package.0, Box::new(package.1));
        }

        for i in 0..cores {
            ch_output.push_str(destination.remove(i).as_ref())
        }
      // here ends the problem of threading    
     println!("{:?}", &outputname);    
 
     let off = std::fs::OpenOptions::new()
                                    .write(true)
                                    .read(false)
                                    .create(true)
                                    .open(outputname);
     let mut of: std::fs::File = off.unwrap();
     let write_result = of.write_all(&ch_output.into_bytes()[..]);
     
     match write_result {
        Ok(value) =>  println!("Wrote file contents, with file size: {:?}", value),
        Err(err) => println!("Error occured while writing contents: {:?}", err),
    }
 
     // done
}
#[derive(Debug)]
struct Trigram<'a> {
    word: &'a str,
    trigrams: Vec<String>,
    sz: usize,
}

impl<'a> Trigram<'a>{
    fn new(w: &'a str) -> Trigram<'a> {
        use std::ops::Index;
        
        let vector:Vec<char> = w.chars().collect();
        let mut v: Vec<String> = 
//        w.chars()
//        .collect::<Vec<char>>()[..]
        vector[..].windows(3)
            .map(
                |x| x.iter().map(
                    |z| z.to_lowercase()
                        .next()
                        .unwrap())
                .collect())
            .collect();
        v.sort();
        let sz = v.len();
        Trigram {
            word: w.clone(),
            trigrams: v,
            sz: sz,
        }
   }

    fn to_string(&self) -> String {
        use std::vec::IntoIter;
        let mut string = String::new();
        string.push_str(self.word);
        string.push(' ');
        string.push_str(self.sz.to_string().as_ref()); 
        string.push(' ');
        let iter = self.trigrams.clone();
        let iter2 = iter.into_iter();
        for w in iter2 {
            string.push_str(w.as_ref());
            string.push(' ');
        }
        string.push('\n');
        string
    }
}
