#include <string> 
#include <vector> 
#include <fstream> 
#include <iostream> 
#include <algorithm> 
#include "./headers/dictionary.h"
#include "./headers/word.h"

using namespace std; 
 
Dictionary::~Dictionary() { 
} 

template <typename A> 
auto min(A const& a, A const& b, A const& c) -> decltype(a + b + c) { 
	  // if (a < b) return a < c else return (b < c)  
  return std::min(std::min(a, b), c);   
} 


// Takes a non-preprocessed dictionary file and generates a dictionary from that 
Dictionary::Dictionary() {
	std::ifstream in{"./d.dict"};
	std::vector<std::string> strwords;
	for(std::string word;std::getline(in, word);)
	{
		if (word.length() < Dictionary::WLEN_SZ) {
		std::transform(word.begin(), word.end(), word.begin(), ::tolower);
		words[word.length()].push_back(Word{word, std::forward<std::vector<std::string>>(Dictionary::gen_trigram(word))});
		} 
	}
    in.close();
}

std::vector<std::string> Dictionary::gen_trigram(const std::string& w)  
{ 
	std::string::const_iterator citr = w.begin(); 
	++citr; ++citr; 
	auto substart = 0; 
	std::vector<std::string> trivec;
	if(w.length() > 3) 
		for(std::string::const_iterator c_iter = w.cbegin(); 
		    citr != w.cend();
		    ++c_iter, ++citr) 
		trivec.push_back(w.substr(substart++, 3)); 
	else trivec.push_back(w);  
  // rad 43
std::sort(trivec.begin(), trivec.end());
	return trivec; 
} 
 
bool Dictionary::contains(const string& word) const { 
//	if(std::find(words[word.length()].begin(), words[word.length()].end(), word) != words[word.length()].end())
//	return true;
 	for(const auto& dword_iter : words[word.length()]) {
		if(dword_iter.get_word().compare(word) == 0) return true;
	}
  return false; 
} 
 
std::vector<string> Dictionary::get_suggestions(const string& word){ 
	vector<string> suggestions; 
	add_trigram_suggestions(suggestions, word); 
	rank_suggestions(suggestions, word);
	trim_suggestions(suggestions);
	return suggestions; 
}

void Dictionary::rank_suggestions(std::vector<std::string>& s, const std::string& w) {
	std::sort(s.begin(), s.end(), [&](const auto& a, const auto& b) {
		return ldist(a, w) < ldist(b, w);
	});
}

void Dictionary::trim_suggestions(std::vector<std::string> &s) {
	if(s.size() > 10) s.resize(10);
}

void Dictionary::add_trigram_suggestions(std::vector<std::string>& s, std::string w) { 
	  auto t = Dictionary::gen_trigram(w);
	  if(w.length() >=1)
	  for(auto begin = w.length() -1; begin < w.size()+1; ++begin) {
	      for (auto witer = words[begin].cbegin(); witer != words[begin].cend(); ++witer) {
	      if(static_cast< float> (witer->get_matches(t)) /  static_cast< float > (t.size())>=0.333){
		      s.push_back((witer->get_word()));
          }
      }
  }
} 

unsigned int ldist(const std::string& a, const std::string& b){ 
	int d[Dictionary::WLEN_SZ+1][Dictionary::WLEN_SZ+1];
	for(int i = 0; i < Dictionary::WLEN_SZ+1; i++) 
	for(int j = 0; j < Dictionary::WLEN_SZ+1; ++j) 
		d[j][i] = 0;
		
	for(int i =0; i < Dictionary::WLEN_SZ; ++i) {
		d[i][0] = i;   
	}	

	for(int j = 0; j < Dictionary::WLEN_SZ; ++j) {
   		d[0][j] = j;
	}	

	for(int i = 1; i < Dictionary::WLEN_SZ; i++) {
		auto cost = 0;
		for(int j = 1; j < Dictionary::WLEN_SZ; ++j) {
			if(a[i-1] == b[j-1]) cost = 0;
			else cost = 1;

    	d[i][j]	=  min(d[i-1][j] + 1, d[i][j-1]+1, d[i-1][j-1] + cost);
 	}	
   }
  return d[a.length()][b.length()];
} 
