#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <string>
#include <vector>
#include <unordered_set>
#include <utility>

#include <memory>
#include "word.h"

unsigned int ldist(const std::string& a, const std::string& b);
std::vector<std::string> gen_trigram(const std::string& w);
class Dictionary {
	static auto constexpr WLEN_SZ = 25;
public:
	Dictionary();
	Dictionary(char* file);
	~Dictionary();
	
	bool contains(const std::string& word) const;
	std::vector<std::string> gen_trigram(const std::string& w);
	std::vector<std::string> get_suggestions(const std::string& word);
	void trim_suggestions(std::vector<std::string>& s);
	void add_trigram_suggestions(std::vector<std::string>& s, std::string w);
	void rank_suggestions(std::vector<std::string>& s, const std::string& w);

	friend unsigned int ldist(const std::string& a, const std::string& b);
	
private:
        std::vector<Word> words[WLEN_SZ];
};

#endif
