#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "headers/dictionary.h"
#include <cctype>
#include <algorithm>

using namespace std;

std::vector<std::string> make_trigram(const std::string& w)
{
	std::string::const_iterator citr = w.begin();
	++citr; ++citr;
	auto substart = 0;
	std::vector<std::string> trivec;
	if(w.length() > 3)
		for(std::string::const_iterator c_iter = w.cbegin();
			citr != w.cend();
			++c_iter, ++citr)
			trivec.push_back(w.substr(substart++, 3));
	else trivec.push_back(w);

	// rad 43
	std::sort(trivec.begin(), trivec.end());
	return trivec;
}

void pre_process(char* file) {
	std::ifstream in;
	in.open(file, std::ios::in);
	std::vector<std::string> output;

	for(std::string word; std::getline(in, word);) {
		// std::transform(word.begin(), word.end(), word.begin(), ::tolower);
		std::vector<std::string> tri = make_trigram(word);
		std::sort(tri.begin(), tri.end());
		std::string str_out {word};

		str_out = str_out + " " + std::to_string(tri.size());

		for(auto ref :  tri) {
			str_out = str_out + " " + ref;
		}
		output.push_back(str_out);
	}
	std::ofstream out;
	out.open("pre_processed.dict", std::ios::out);
	if(out.is_open())
	for(const auto& line : output) {
		out << line << std::endl;
	}
	out.close();
}

int main(int argc, char** argv) {
	if(argc > 1) {
		pre_process(argv[1]);
	}
}

