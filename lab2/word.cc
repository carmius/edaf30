#include <string> 
#include <vector> 
#include "./headers/word.h"
#include <iostream> 
using namespace std; 

Word::~Word() {

}

const std::string Word::get_word() const { 
	  return word; 
} 

const std::vector<std::string> Word::get_tri() const {
  return trigrams;
}
unsigned int Word::w_sz() { return word.size(); } 
unsigned int Word::set_sz() { return trigrams.size(); } 

unsigned int Word::get_matches(const vector<string>& t) const  
{ 
  auto res = 0; 
 
  auto outer_it = trigrams.cbegin();
  auto inner_it = t.cbegin();
  auto increment_it = t.cbegin();
 
  for(; outer_it != trigrams.cend(); ++outer_it) {
  	for(inner_it = increment_it; inner_it != t.cend(); ++inner_it) {
		if(*outer_it == *inner_it) {
			res++;
			increment_it = inner_it;
		}
	}
  }
return res;

}

bool Word::operator==(const Word& rhs) const {
	return this->word == rhs.word;
}

