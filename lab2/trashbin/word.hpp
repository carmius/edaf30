#ifndef WORD_H
#define WORD_H

#include <string>
#include <vector>

using namespace std;

class Word {
public:
	/* Creates a word w with the sorted trigrams t */
	Word(const std::string& w, const std::vector<std::string>& t): word{w}, trigrams{t} {};
    ~Word();
	/* Returns the word */
	const std::string get_word() const;
	const std::vector<std::string> get_tri() const;
	/* Returns how many of the trigrams in t that are present
	 in this word's trigram vector */
	unsigned int get_matches(const std::vector<std::string>& t) const;
	unsigned int set_sz();
	unsigned int w_sz();
private:
	std::string word;
	std::vector<std::string> trigrams;
};

#endif
