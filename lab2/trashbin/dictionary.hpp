#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <string>
#include <vector>
#include <unordered_set>
#include <utility>

#include <memory>
#include "word.h"

class Dictionary {
public:

	Dictionary();
	~Dictionary();

	bool contains(const std::string& word) const;
	std::vector<std::string> gen_trigram(const std::string& w);
	std::vector<std::string> get_suggestions(const std::string& word);
	void trim_suggestions(std::vector<std::string>& s);
	void add_trigram_suggestions(std::vector<std::string>& s, std::string w);
	void rank_suggestions(std::vector<std::string>& s, const std::string& w);

	unsigned int ldist(const std::string& a, const std::string& b);
	unsigned int dldist(std::string& a, std::string& b);

	// Define a generic min fn  using std::min 
	template <typename A> 
	auto min(A const& a, A const& b, A const& c) -> decltype(a + b + c) { 
	  // if (a < b) return a < c else return (b < c)  
	  return std::min(std::min(a, b), c);   
	} 
 
	
private:
    static constexpr auto WLEN_SZ = 25;
    std::unordered_set<std::string> word_set;
    std::vector<Word> words[25];
    	    // std::vector<Word> words[WLEN_SZ+1];

	static constexpr auto wordsz_range = 1;

};

#endif
