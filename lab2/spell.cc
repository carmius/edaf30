#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <cctype>
#include "./headers/dictionary.h"
#include "./headers/word.h"

int main(int argc, char** argv) {
	Dictionary dict;

	std::string word;
	while (std::cin >> word) {
		if(word.substr(0,2) == "-p") {
			auto n_word = word.substr(2, word.length()-2);
			Word W(n_word, dict.gen_trigram(n_word));
			std::cout << "Printing trigrams in-order for: " << n_word << std::endl;
			for(auto w : W.get_tri()) {
				std::cout << w << '\t';
			}
		} else if(word.substr(0,2) == "-c") {
			std::cout << "Compare two words and their ldistance cost: \n";
			std::string first, second;
			std::cin >> first;
			std::cout << "Input 2nd word: \n";
			std::cin >> second;
			Word f(first, dict.gen_trigram(first));
			Word s(second, dict.gen_trigram(second));
			auto cost = ldist(first, second);
			std::cout << "Cost: " << cost << '\n';
			for(auto& tri : f.get_tri()) {
				std::cout << tri << "\t";
			}
			std::cout << "\n";
			for(auto& tri : s.get_tri()) {
				std::cout << tri << "\t";
			}
		}
		else if(word.substr(0,2) == "-q") {
			return 0;
		}
		else {
			std::transform(word.begin(), word.end(), word.begin(), ::tolower);
			if (dict.contains(word)) {
				std::cout << "Correct." << std::endl;
			} else {
				std::vector<std::string> suggestions = dict.get_suggestions(word);
				if (suggestions.empty()) {
				std::cout << "Wrong, no suggestions." << std::endl;
				}
			       	else {
					std::cout << "Wrong. Suggestions:" << std::endl;
					for (const auto& w : suggestions) std::cout << "    " << w << std::endl;	
				}
			}
		}
	}
	return 0;
}
