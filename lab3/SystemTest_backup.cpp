#include <iostream>
#include "User.h"
#include "UserTable.h"
#include <functional>
#include <typeinfo>
#include <vector>
#include <memory>
// #include "benchmark/include/benchmark/benchmark.h"
/*  Create such a test program (including a main function) that
 *  • creates a UserTable object,
    • calls the function find(int) to find the user with card number 21330,
    • calls the function find(int) to find the user with card number 21331 and
    • calls the function find(std::string) to find the user named ”Jens Holmgren”.


    Bug (file:lineno)                 Describe the problem.                                                     How did you find the bug?

    UserTable.cpp: In member function ‘User UserTable::find(int) const’:                                                    compiler warning
    UserTable.cpp:65:23: warning: suggest parentheses around assignment used as truth value [-Wparentheses]                 compiler warning
                                       if (midnbr = c) {



 */

constexpr uint8_t TEST1 = 3;
constexpr uint8_t TEST2 = 10;

struct Credential {
    unsigned int test_no;
    Credential(const User& usr, const User& cred) : act(usr), exp(cred) {};
    ~Credential() {};
    bool checkCredentials() {
        return (passed = (act == exp));
    }
    User act, exp;
    bool passed;
};

// bool checkCredentials(const User& usr, const User& cred) {
//    return usr == cred;
//}

int main() {

    UserTable tbl{"./users.txt"};
    UserTable test_case{"./users_testcase.txt"};
    static int passed_tests = 0;


    // ville bara leka med std::unique_ptr, testa hur man passar en deleter till pekaren.
    auto TestLog = [&](Credential* c) {
        std::cout << "Running test " << c->test_no << ": ";
        if(c->passed) {
            passed_tests++;
            std::cout << "passed \n";
            std::cout << "Actual values: " << c->act;
            std::cout << "Expected values" << c->exp << std::endl;
        } else {
            std::cout << "failed \n";
            std::cout << "Actual values: " << c->act;
            std::cout << "Expected values" << c->exp << std::endl;
        }
        delete(c);
    };

    std::cout << "Numbers of stored users in system: " << tbl.getNbrUsers() << std::endl;
// request user credentials using system

    std::string helo = "Jens Holmgren";
    auto u = tbl.find("Jens Holmgren");

    User task1[TEST1] = {
            tbl.find(21330),
            tbl.find(21331),
            tbl.find(helo),
    };

    std::vector<std::unique_ptr<User>> scenario2{};
    scenario2.push_back((std::unique_ptr<User>(new User{1, "Adam Abrahamsson"})));
    scenario2.push_back((std::unique_ptr<User>(new User{2, "BAgnes Abrahamsson"})));
    scenario2.push_back((std::unique_ptr<User>(new User{3, "CAgneta Abrahamsson"})));
    scenario2.push_back((std::unique_ptr<User>(new User{4, "DAlbin Abrahamsson"})));
    scenario2.push_back((std::unique_ptr<User>(new User{6, "EAlexander Abrahamsson"})));
    scenario2.push_back((std::unique_ptr<User>(new User{7, "FAlexandra Abrahamsson"})));
    scenario2.push_back((std::unique_ptr<User>(new User{8, "Glice Abrahamsson"})));
    scenario2.push_back((std::unique_ptr<User>(new User{9, "Hlva Abrahamsson"})));
    scenario2.push_back((std::unique_ptr<User>(new User{10, "Imanda Abrahamsson"})));
    scenario2.push_back((std::unique_ptr<User>(new User{11, "JAnders Abrahamsson"})));

// user credential test cases
    std::unique_ptr<User> scenario1[TEST1] = {
        std::unique_ptr<User>(new User{21330, "Irene Danielsson"}),
        std::unique_ptr<User>(new User{21331, "Tony Hansen"}),
        std::unique_ptr<User>(new User{97368, "Jens Holmgren"})
    };

    for(int i = 0; i < TEST1; i++) {
        std::cout << "Running test " << i << ": ";
        std::unique_ptr<Credential, decltype(TestLog)> CredentialCheck(std::move(new Credential(task1[i], *scenario1[i])), TestLog);
        CredentialCheck->test_no = i;
        CredentialCheck->checkCredentials();
        // std::cout << "Running test " << i << ": " << (checkCredentials(task1[i], *scenario1[i]) ? "passed" : "failed") << std::endl;
    }

    int j = 0;
        for(int i = 0; i <= TEST2; i++, j++) {
        if(i == 4) ++i;
	    auto c = new Credential(test_case.find(i+1), *scenario2[j]);
	    std::unique_ptr<Credential, decltype(TestLog)> CredentialCheck(std::move(c),TestLog);
            CredentialCheck->test_no = i;
            CredentialCheck->checkCredentials();
        }

    std::cout << "Users in testcase: " << test_case.getNbrUsers() << std::endl;

    test_case.addUser(User{1234, "Simon Farre"});
    std::cout << "System tests passed: " << passed_tests << " out of " << TEST1 + TEST2 << std::endl;
    std::cout << "Testing add/print functions : " << test_case.find(1234) << std::endl;
    // std::cout << "testFindNbr() : " << tbl.testFindNbr() << std::endl;
    // auto somefuckingvalue = UserTable::testFindNbr(tbl);
    auto somefuckingvalue2 = testFindNbr(tbl);
    std::cout << "testFindNbr() : " << somefuckingvalue2 << std::endl;

    // test_case.print(std::cout);
    // std::cout << tbl.find(21330);
    return 0;
}
