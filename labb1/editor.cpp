
#include "editor.h"
#include <string>
#include <vector>
#include <iostream>

using std::string;
using size_type = std::string::size_type;

size_type Editor::get_size() const
{
    return text.size();
}

//	Editor ed("...(...(...[...]...)...)...{...}...");
// this is a function which will use LIFO when finding (....{...[.(.(..).)..]...}...)
size_type Editor::find_left_par(size_type pos) const {
	using string = std::string;
    std::string search = text.substr(0, pos);

    // brackets, curly braces or parens?
    char str = text.at(pos);
	char key = 'X'; // following the RAII rule
	switch(str) {
		case ']':
			key = '[';
			break;
		case '}':
			key = '{';
			break;
		case ')':
			key = '(';
			break;
	}

    // safety measure if there's no right parens to begin with
    if(key == 'X') return std::string::npos;

    // match correctly
    auto result = pos;
    int count = 1;


    for(string::iterator it = search.end(); it != search.begin(); --it, --result) {
        if(key == *it) {
             count--;
                if(count == 0) return result;
        }
        else if(*it == str) {
            count++;
        }
    }
    return std::string::npos;
}
