#include <iostream>
#include <string>

std::string process_word(std::string& w) {
	std::string word_to_be{w};
	return word_to_be;
}

struct Word {
	Word(std::string word): w(word) {};
	~Word() {
	
	}
	
	std::string w;

};

int main(){
	std::string s1 = "Word 1";
	std::string s2 = "Word 2";
	Word w1{process_word(s1)};
	Word w2{process_word(s2)};
	std::cout << "Word 1, w/o move semantics: " << &w1;
	std::cout << "Word 2, w/ move semantics: " << &w2;

	return 0;
}



